FROM python:3.9-alpine

RUN apk update && apk add gcc musl-dev

COPY requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /mnt
CMD ["mkdocs",  "serve", "-a", "0.0.0.0:8000"]
