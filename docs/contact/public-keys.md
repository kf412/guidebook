title: Sensitive content

# Sending us sensitive content using GPG

The [GPG public keyring](../downloads/teampubkeys.gpg) for the DevOps team can
be used to securely send files or emails to us.

## Encrypt a file online

You can use this tool to securely encrypt a file to send to us via email. The
data does not leave your web browser. All encryption is done entirely on your
own device.

Firstly, select recipients for the file:

<div style="margin-left: 4ex;">
  <select id="key-select" name="keys" multiple>
  </select>
  <div>
    <em>Hint: use Ctrl or Option key to select multiple recipients.</em>
  </div>
  <p>
    <button id="key-select-all" type="button" class="md-button md-button--primary">Select all</button>
    <button id="key-select-none" type="button" class="md-button">Clear selection</button>
  </p>
</div>

Secondly, paste the contents of the file:

<div style="margin-left: 4ex;">
  <textarea id="file-contents" name="contents" rows="10" cols="80" placeholder="Paste file content here.">
  </textarea>
</div>

And finally send us the encrypted contents using the appropriate method listed
on the [contact page](./index.md).

<div style="margin-left: 4ex;">
  <textarea id="armoured-result" readonly name="result" rows="10" cols="80"></textarea>
  <p>
    <button id="copy-result" type="button" class="md-button md-button--primary">Copy to clipboard</button>
    <span style="margin-left: 2ex;" id="copy-confirm"><span>
  </p>
</div>

## Encrypting secrets via the command line

Here are some handy commands to keep around for using
[GPG](https://en.wikipedia.org/wiki/GNU_Privacy_Guard) to communicate with us.
These instructions assume that:

1. you have GPG installed and
2. are relatively familiar with using the command line.

### Listing contents of the public keyring

Download our [GPG public keyring](../downloads/teampubkeys.gpg) and use the
following command:

```sh
gpg --list-keys --no-default-keyring --keyring ./teampubkeys.gpg
```

### Encrypting a file to send to us

Download our [GPG public keyring](../downloads/teampubkeys.gpg) and use the
following command to encrypt a file named `foo.txt` so that `spqr1@cam.ac.uk`
and `jo.example@uis.cam.ac.uk` can decrypt it:

```sh
gpg --no-default-keyring --keyring ./teampubkeys.gpg --armour \
  --recipient spqr1@cam.ac.uk \
  --recipient jo.example@uis.cam.ac.uk \
  --encrypt foo.txt
```

!!! note
    The recipient email address must match those listed in the public keyring.
    The use of `--no-default-keyring` helps ensure that the key you use for a
    recipient is one from our team keyring and not a personal key.

## Updating the public key ring

If you're a DevOps division member, you can open a merge request on the
[guidebook
project](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/) in
GitLab updating `docs/downloads/teampubkeys.gpg`. To import your _public_ key
into the keyring:

```sh
gpg --no-default-keyring --keyring ./teampubkeys.gpg --import [KEYFILE]
```

where `[KEYFILE]` is your public key. The GPG manual has [a page on extracting
your public key](https://www.gnupg.org/gph/en/manual/x56.html).

<!--
  We vendor our own OpenPGP.js copy to ensure we know the precise version.
-->
<script src="../openpgp.min.js"></script>
<script src="../encrypt.js"></script>
