# DNS

This page documents how we configure DNS zones for our deployments. It also
discusses how we verify ownership of domains under `cam.ac.uk` with Google.

## Zone delegation

The DNS zone `gcp.uis.cam.ac.uk` is delegated to a Google Cloud DNS hosted zone
managed by
[gcp-infra](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/)
(University members only).

When a new product is added, a *product zone* is created by configuration in
[dns.tf](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/-/blob/master/product/dns.tf).
This will be of the form `[product-slug].gcp.uis.cam.ac.uk`. The _product admin_
service account is granted the ability to add records to this zone. DNSSEC
records are added as appropriate.

Per-product DNS configuration is handled by [our standard
boilerplate](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/blob/master/%7B%7B%20cookiecutter.product_slug%20%7D%7D-deploy/dns.tf)
(University only). The _product admin_ account is used to create a
per-environment zone which the _project admin_ account can add records to. For
example, the "development" environment for the "example" product would have the
zone `devel.example.gcp.uis.cam.ac.uk` created. Again, DNSSEC records are
added.

At this point, the _project admin_ can freely add records to the
environment-specific zone.

!!! example "Example: Load Balancer IPs"

    When configuring the [Cloud Load
    Balancer](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/blob/master/%7B%7B%20cookiecutter.product_slug%20%7D%7D-deploy/webapp_load_balancer.tf)
    in our boilerplate, we create an "A" record pointing to the load balancer's
    IP with the following config:

    ```tf
    # DNS records for webapp. For load-balanced applications, these are created
    # irrespective of the any custom DNS name. For custom DNS name-hosted webapps,
    # you will probably need a further CNAME record pointing to this record.
    resource "google_dns_record_set" "load_balancer_webapp" {
      count = local.webapp_use_cloud_load_balancer ? 1 : 0

      managed_zone = google_dns_managed_zone.project.name

      ttl  = 300
      name = local.webapp_dns_name
      type = "A"
      rrdatas = [
        module.webapp_http_load_balancer[0].external_ip
      ]
    }
    ```

## Adding records under `.cam.ac.uk`

The existing [IP register database](https://www.dns.cam.ac.uk/) is used to
provision records under `.cam.ac.uk`. At the moment this cannot easily be
automated.

!!! tip "The IP-register service"

    New team members will almost certainly want to read the [IP register
    documentation](https://www.dns.cam.ac.uk/ipreg/docs.html) since the system
    is bespoke to Cambridge. Unfortunately there's not much substitute for
    seeing someone do this the first time and so, if you've not done this
    before, it's well worth shadowing another team member when the following
    procedure is next performed.

To add records you will need to liaise with
[ip-register@uis.cam.ac.uk](mailto:ip-register@uis.cam.ac.uk) to ensure the
following:

* The zone containing the record you wish to register is in the `UIS-DEVOPS`
    _mzone_. (For example, if you want to register `foo.raven.cam.ac.uk` then
    `raven.cam.ac.uk` needs to be in the mzone.)
* You have the ability to administer the UIS-DEVOPS mzone in the IP register
    database. The list of administrators for the mzone can be listed via the
    [single_ops page](https://jackdaw.cam.ac.uk/ipreg/single_ops).

Our general approach is to add whatever records we need for the service within
the product environment specific zone under `gcp.uis.cam.ac.uk` and add CNAME
records for the `.cam.ac.uk` "friendly" names.

Adding a CNAME is a bit of a fiddle:

* In [vbox_ops](https://jackdaw.cam.ac.uk/ipreg/vbox_ops) add a vbox which
    corresponds to the `.gcp.uis.cam.ac.uk` host name.
* In [cname_ops](https://jackdaw.cam.ac.uk/ipreg/cname_ops) add a CNAME which
    points to the `.gcp.uis.cam.ac.uk` host name.

## TLS Certificates and Domain Verification

Google Cloud services which can host web content generally have two options for
TLS certificates:

* **Self-managed certificates** require that one generate private keys and
  corresponding TLS certificates signed by some appropriate trust root "out of
  band" and provide the key and certificate to the service. They can be a
  management burden since one needs to make sure that appropriate procedures are
  in place to renew and replace certificates before expiry. As such we are
  incentivised to have long-lived certificates.

* **Google managed certificates** are issued and managed as part of the Google
  Cloud platform. They are automatically renewed and replaced as necessary. Like
  many automated certificate provisioning solutions, Google managed certificates
  tend to be short-lived, typically 90 days. Renewing and rotating TLS
  certificates often is desirable from a security hygiene perspective. This,
  coupled with the lower maintenance burden, means we generally use Google
  managed certificates where possible.

In order for Google to issue certificates, you must verify ownership of the
domain.

### Domains under `.gcp.uis.cam.ac.uk`

Our boilerplate includes semi-automated verification for domains under
`.gcp.uis.cam.ac.uk` by means of verification records.

We will use the example domain `devel.example.gcp.uis.cam.ac.uk` in this
section. This section also assumes the product's terraform configuration has
already been `apply`-ed at least once.

Domain verification is started using the [gcloud
tool](https://cloud.google.com/sdk/install):

```console
gcloud domains verify devel.example.gcp.uis.cam.ac.uk
```

A browser window will appear with the Google Webmaster tools page shown.

1. Open a new browser window in incognito mode and enter the URL shown
   previously. (The Chrome browser on Macs will attempt to set up
   synchronisation if one signs in to a Google account outside of a private
   browsing session.)
2. Click the avatar in the top-right corner and make sure that you are signed in
   as the UIS DevOps bot user, devops@uis.cam.ac.uk. Credentials for this user
   can be found in 1Password.
3. Select **Other** as a domain name provider and click the **Add a CNAME
   record** link.
4. You will be asked to add a CNAME record of the form
   `[HOST].devel.example.gcp.uis.cam.ac.uk` pointing to some target.
   Add the `[HOST]` part and target to the `workspace_domain_verification`
   section of
   [locals.tf](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/blob/master/%7B%7B%20cookiecutter.product_slug%20%7D%7D-deploy/locals.tf). For example, if your CNAME host
   was `abc1234.devel.example.gcp.uis.cam.ac.uk` and the target was
   `gv-XXXXXX.dv.googlehosted.com`, set `cname_host` to `abc1234` and `cname_target` to
   `gv-XXXXXX.dv.googlehosted.com`.
5. Apply the configuration so that the verification CNAME record is created.
6. It will take up to 5 minutes for Google's DNS to start serving the CNAME
   record. Make a cup of coffee and then click **Verify** to verify ownership.
7. When verification is successful, click **Add additional owners to
   devel.example.gcp.uis.cam.ac.uk.**, add add the *project admin
   service account* email address. This is available in the
   `project_admin_service_account_email` terraform output.
8. Add `verified = true` to the workspace's domain verification state in
   `workspace_domain_verification` in locals.tf.
9. Apply the configuration again.

### Domains under `.cam.ac.uk`

Unfortunately, we cannot yet semi-automate the verification of domains outside
of `gcp.uis.cam.ac.uk`. For all other `.cam.ac.uk` domains we need to perform
manual verification.

The G Suite admins can manually verify ownership of domains under `.cam.ac.uk`
with Google. They will need:

* The email address of the _project admin_ service account.
* The domain to verify.

One can contact the G Suite admins via
[gapps-admin@uis.cam.ac.uk](mailto:gapps-admin@uis.cam.ac.uk). Since all G Suite
admins are also in DevOps, you might find it quicker to ask in the divisional
Google Chat.

!!! info

    [A
    recording](https://drive.google.com/file/d/1TAPkz971dVXLma4jUebcxFoikCP9dHE9/view)
    (DevOpd only) of a meeting where a G Suite admin demonstrated how this
    is done is available in the DevOps shared drive.

## Summary

In summary:

* Domains under `.gcp.uis.cam.ac.uk` are managed via Google's Cloud DNS service.
* Each product receives a dedicated zone named `[PRODUCT].gcp.uis.cam.ac.uk`
    which can be administered by the _product admin_ service account.
* Each environment is provisioned with a dedicated zone named
    `[ENVIRONMENT].[PRODUCT].gcp.uis.cam.ac.uk` which can be administered by the
    _project admin_ service account.
* All zones have appropriate DNSSEC records created.
* Domain ownership must be verified for Google to issue TLS certificates.
* Verification of domain ownership is semi-automated for domains under
    `.gcp.uis.cam.ac.uk` but must be performed manually by a G Suite admin for
    other `.cam.ac.uk` domains.
