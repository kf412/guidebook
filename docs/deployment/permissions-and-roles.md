# Permissions and Roles

This page documents how we manage personal and service account permissions.

## Permissions granted by gcp-infra

As part of the [product provisioning process](./gcp-folders.md), we have a
product folder and product meta project created automatically for each product.

A *product admin* service account is created in the meta project and has owner
rights over all projects in the product folder. Additionally the product admin
can create new projects within the folder and associate them with the University
Billing Account.

Access is granted to individual team members at one of three levels:

* **Viewer** These users have "viewer" rights over all projects in the product
    folder. They can inspect the deployment in the Google Cloud console but
    cannot see sensitive resources such as secrets. Giving someone "viewer"
    access is appropriate if they're involved in initial troubleshooting, need
    to see logs or are being shown around the deployment.
* **Deployer** These users have the ability to view
    the value of the Google Secret Manager secret which contains credentials for
    the *product admin*. As such they can run the terraform configuration. Since
    they can access the product admin credentials, they can perform any actions
    the product admin can but only via terraform or the `gcloud` utility; they
    cannot modify things in the Google Cloud Console and can only _see_ things
    in the console if they are also "Viewer" users.
* **Owner** Owner rights over all projects. Can see all resources in the Google
    Cloud Console and modify them. Generally this is given to those directly
    involved in maintaining or creating the product's deployment configuration.

!!! question "Why does 'Deployer' exist?"

    Ideally we'd be able to add a big "elevate your permissions" button to the
    Google Cloud console so that most of the time you're a viewer but you have
    to take decisive action to make changes. The Google Cloud console is very
    powerful and it is easy to "fat-finger" a change when you just meant to look
    around.

    Until users get comfortable with what _is_ and _isn't_ safe to do in the
    console, we make them viewers and deployers. The viewer rights mean that
    they can inspect the current configuration and the deployer rights mean that
    they can make changes but _only_ through terraform or the `gcloud`
    command-line utility. This at least makes one have to actively _decide_ to
    do something which could change something.

    As such, our concept of "deploy user" is more of a "prevent accidental
    change" feature than a security-related feature. Much like passwordless sudo
    on a traditional Unix box it is designed to make it more obvious to an admin
    when they're doing something potentially dangerous.

The association between individual accounts and these roles is specified in the
gcp-infra project. For DevOps we have also made this information available via a
[team data
module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/)
(University members only).

!!! note "Future improvements"

    Since this system was developed, Google Cloud gained knowledge of [Lookup
    groups](https://www.lookup.cam.ac.uk/). As such we can also start assigning
    these roles to members of Lookup groups by granting permissions to
    the `[GROUPID]@groups.lookup.cam.ac.uk` Google Group.

    This improvement is being tracked in [an
    issue](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/-/issues/55)
    on the Developers' Hub (DevOps only).

## Per-environment permissions

Some service accounts and permissions are set at an environment level rather
than at a product level.

### Project admin

Our terraform configurations usually create a *project admin* service account
which is given owner rights over the environment-specific Google Project but
cannot affect resources in other Google Projects. This is usually a safer
service account identity to use and is the identity we associate with the
default `google` and `google-beta` providers.

This service account is created [within the gcp-project terraform
module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-project/-/blob/master/main.tf#L29).

### Cloud Run Deployer

For [continuous deployment](./continuous-deployment.md) we make use of a service
account which has rights only to deploy new revisions within an existing Cloud
Run service, not to create new ones or delete existing ones.

We do this by means of a custom Cloud IAM role created with the following
terraform:

```tf
resource "google_project_iam_custom_role" "run_editor" {
  role_id     = "runEditor"
  title       = "Run Editor"
  description = "Update existing Cloud Run deployments"
  permissions = [
    "run.services.get",
    "run.services.list",
    "run.services.update",
    "run.routes.get",
    "run.routes.list",
    "run.configurations.get",
    "run.configurations.list",
    "run.revisions.get",
    "run.revisions.list",
    "run.locations.get",
    "run.locations.list",
  ]
}
```

### Service identities

All processes within Google Cloud have an identity associated with them. There
is usually a default identity for each service. For example, unless configured
otherwise, applications running via Cloud Run have the [Compute Engine default
service account
identity](https://cloud.google.com/run/docs/securing/service-identity).

To allow granular control we try to configure all processes to have dedicated
service accounts. In particular you should try to create custom service accounts
for:

* Cloud Run services,
* Cloud Function functions, and
* Kubernetes workloads via [GKE workload
    identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity).

!!! example

    Within the [Raven
    infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/raven/infrastructure/-/blob/master/raven_stats.tf)
    (DevOps only) we create a Cloud Function which fetches device statistics via
    the Google Analytics API and stores a summary report in a Cloud Storage
    bucket. This function runs as a custom service account which is given rights
    to update the summary in the bucket and is added to our Google Analytics
    property as a read-only user.

## Summary

In summary,

* Each *product* has a *product admin* service account which has "Owner" rights
    over all projects in the product folder.
* For real users, as opposed to service accounts, per *product* permissions are
    "Owner", "Deployer" and "Viewer". "Owners" can
    do everything, "Deployers" can run terraform and use the `gcloud` CLI to do
    anything and "Viewers" can see how infrastructure is configured but not some
    sensitive resources such as secrets.
* There is one Google project per environment. For example, one project for
    "production", one for "staging", etc.
* Each environment-specific Google project has a *project admin* service account
    which has "owner" rights over that project.
* The default terraform provider uses the *project admin* service account
    credentials.
* We create custom Cloud IAM roles to allow continuous deployment jobs to deploy
    new versions but not create new services or delete old ones.
* We try to explicitly create custom service account identities for all
    processes.
