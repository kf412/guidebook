# Meeting room locations

UIS is located mostly within the Roger Needham Building which has a number of
meeting rooms available. The meeting rooms are mostly named after towns in
Cambridgeshire. This page maps the toponymic room names to their location within
the building.

| Name | Code | Location |
| --- | --- | --- |
| Ely 1 & 2 | GS57 & GS51 | Ground floor, next to vending machines |
| Cambourne | 1S21 | First floor, South side, end of row of glass-sided offices |
| Huntingdon | GS67 | Ground floor, next to vending machines |
| Interview & Records Room | 1N65 | First floor, outside 1N08 |
| Newmarket | 1S11 | First floor, by entrance to corridor |
| Norwich | GN01 | Ground floor, next to reception |
| Peterborough | 1S03 | First floor, South side, opposite kitchen |
| RIS Resource Room | 2S13 | Second floor, South side, opposite toilets |
| Swaffham | GN13 | Ground floor, next to card office |
| Thetford | GS01 | Ground floor, next to reception |
| Video & Planning Room | 2N13 | Second floor, with window |
| Wisbech | 2N58 | Second floor, windowless |
