---
title: Overview
---

# A Guide to the DevOps Division

## Introduction

This is the UIS DevOps Division guidebook. It provides a central place for us to
write down our processes, document the services we provide and to discuss some
of our best practice.

## About us

### Who we are

We're a division of the University of Cambridge [Information
Services](https://www.uis.cam.ac.uk/) (UIS).  You can see who makes up the
DevOps division on
[Lookup](https://www.lookup.cam.ac.uk/group/uis-devops/members).

### What we do

Each [service which we provide](services/index.md) has its own page in the
guidebook. For specific information on a service, look there.

### Contacting us

You can contact the team by emailing
[devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk). More information is
available in the dedicated [contact section](contact/) including information on
how to download the team's [GPG public keys](contact/public-keys.md).

## The guidebook

This guidebook documents what we as a team in DevOps aspire to be, what we
currently do and practices we currently find to be helpful.

It is not intended as a prescriptive document but rather to be *descriptive* of
the general way we do things. If the way we do things and the book disagree, it
is the book which is usually at fault.

Occasionally we find ourselves writing guides to ourselves or to others about
how we do things and learnings from mistakes we've made. The [notes
section](notes/index.md) of the guidebook provides a single place for these
notes to live.

### Updating the guidebook

Anyone with a Raven account may propose an edit to the guide by opening a merge
request on the [guidebook
project](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/) in the
University [Developers' Hub](https://gitlab.developers.cam.ac.uk/). Each page in
the guidebook has an edit link which takes you directly to the corresponding
page in the guidebook project.
