title: Domain verification

# Domain verification for the Google Cloud platform

When deploying services to Google Cloud platform (GCP), you will usually want
them accessible as "friendly" names under `cam.ac.uk`.

!!! important
    The contents of this note have moved to our [description of deployments to
    Google Cloud](../deployment/dns.md).
