# API Gateway

This page gives an overview of the API Gateway service.

## Service Description

The API Gateway service allows people to publish APIs via
https://api.apps.cam.ac.uk/ which make use of common quota enforcement,
developer registration and authentication.

A developer portal allowing self-service registration of applications which make
use of the published APIs is at https://developer.api.apps.cam.ac.uk/.

## Service Status

This service is currently an *ALPHA* offering.

## Contact

Issues with the service should be raised on the [corresponding GitLab
project](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops)
(University members only).

## Environments

The API Gateway is hosted by our vendor, Apigee.

## Source code

Terraform which drives the API Gateway is developed in a [dedicated project on
GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops).

## Technologies used

The following gives an overview of the technologies the API Gateway is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Configuration | Apigee proprietary | N/A |
| Deployment | terraform | N/A |

## Deployment

Deployment is via terraform using our
[logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/) tool. There
are currently two terraform workspaces:

* `development` which manages a testing Apigee organisation named
    "devopsaccountrecoveryapigeestaging-eval"
* `production` which manages the production Apigee organisation named "cam"

## Documentation

Dedicated operational documentation is hosted [within the GitLab
project](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops/-/tree/master/doc).

End-user documentation is hosted on the [Developer
portal](https://developer.api.apps.cam.ac.uk/).

## Service Management and tech lead

The **service owner** for the API gateway is currently vacant.

The **service manager** for the API gateway is currently vacant.

The **tech lead** for the API gateway is [Rich
Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The following engineers have operational experience with the API Gateway and are able
to respond to support requests or incidents:

* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23)
