# Card Management System

This page gives an overview of the Card Management System, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    This service as a whole is currently pre-alpha and is **not usable in production**.

## Service Description

The Card Management System is a replacement for the existing
[legacy card database](./legacy-card-db.md). The Card Management System will allow the management
of university cards - from the creation and printing of university cards, through to the export
of data for use by downstream systems.

## Service Status

The Card Management System as a whole is currently `pre-alpha`, although the `Card API` has
moved to `alpha` for testing with a limited number of users.

The Card Management System is under active development, details of the project can be found
within the
[project page on the UIS site](https://www.uis.cam.ac.uk/it-portfolios/projects/card-system-replacement).

## Contact

Technical queries and support should be directed to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) rather than reaching out to team members directly.

Queries about the project or functionality of the Card Management System should be raised as
[GitLab issues in the feedback repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/feedback).

Technical questions or problems with the Card API should be raised as
[GitLab issues in the Card API repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api).

## Environments

The only component of the Card Management System currently deployed is the Card API, which is
deployed behind the [API Gateway](./api-gateway.md):

| Name | URL | Internal GCP url |
| --- | --- | --- |
| Production  | https://api.apps.cam.ac.uk/card/       | https://card.prod.identity.gcp.uis.cam.ac.uk/ |
| Staging     | https://api.apps.cam.ac.uk/card-test/  | https://card.test.identity.gcp.uis.cam.ac.uk/ |
| Development | https://api.apps.cam.ac.uk/card-dev/   | https://card.devel.identity.gcp.uis.cam.ac.uk/  |

API documentation for the production environment can be found within the
[API Gateway's developer portal](https://developer.api.apps.cam.ac.uk/docs/card/1/overview).

Currently the `Production` and `Staging` instances of the Card API serve data from the `CARD`
Oracle database of the [legacy card database](./legacy-card-db.md). The `Development` instance
serves a limited set of data from the `CARD_T` database.

Data is synced from the Oracle databases according to the schedules defined within the
[deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/legacy_data_sync.tf#L3).

## Source code

The source code for the Card Management System is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Card API](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api) | The source code for the Card API - the core API which will serve the frontend and other external services. |
| [Card Client](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-client) | The source code for the client tool which allows data to be exported from the Card API. |
| [Sync Tool](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/sync-tool) | The source code for the tool which allows data to be synced from the card db Oracle databases to the Card API. |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity) | The Terraform infrastructure code for deploying the Card API and related services to GCP. |

## Technologies used

The following gives an overview of the technologies the Card Management System is built on.

| Category | Language | Primary framework |
| -------- | -------- | --------- |
| Card API | Python | Django |
| Card Client | Python | Requests |
| Sync Tool | Python | Oracle_cx |

## Operational documentation

The following gives an overview of how the Card Management System is deployed and maintained.

### How and where the Card Management System is deployed

The infrastructure for the Card Management System is deployed through the Terraform defined
within the [Deploy Identity](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity)
project. The Card API itself is deployed via GitLab pipelines, and will be automatically deployed
to staging on the success of each master pipeline
([see pipeline configuration here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/-/blob/master/.gitlab-ci.yml#L26.)),
with production deployment a manual step in the pipeline once staging has succeeded.

### Deploying a new release

Deploying any infrastructure changes can be completed using
[Logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan)
as described within the
[Deploy Identity project's readme](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/README.md).

Deploying a new release of the Card API to staging and then production can be completed by running
the [master pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/-/pipelines)
within the Card API project. Changes must be tested in staging before deploying to production.

### Monitoring

The Card API makes use of our
[GCP site monitoring module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring)
for monitoring of the production environment. This will send an alert to
devops-alarms@uis.cam.ac.uk if the Card API fails to respond to uptime checks.

### Debugging

Information about how to run and debug the Card API locally are included within the
[Card API's readme](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/-/blob/master/README.md).

## Additional documentation

Information about the overall architecture of the new Card Management System can be found
[within the wiki page for the identity project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation/-/wikis/card/Architecture).

## Service Management and tech lead

The **service owner** for the Card Management System is **currently vacant**.

The **service manager** for the Card Management System is **currently vacant**.

The **tech lead** for the Card Management System is [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23).

The following engineers have operational experience with the Card Management System and are able to
respond to support requests or incidents:

* [Andrew Vella](https://www.lookup.cam.ac.uk/person/crsid/av603)
* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
