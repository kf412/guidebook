# Cloud Research Platform (RONIN)

Simplified AWS resources management for research groups.

## Service Description

The Cloud Research Platform is UIS' response to the need of flexible cloud infrastructure with simplified setup and management. It uses a straightforward web application called RONIN which hides AWS' technical details from the end users.

Its basic operation consists in creating research projects containing a group of users and a budget,  and allowing those users to consume (create, reshape, monitor, stop, destroy) cloud resources from a predefined catalogue in a self-service way.

The application provides instant visual feedback about the existing resources, their statuses and associated costs as well as forecasts and notifications according to budget thresholds. From a management perspective, it allows a detailed expenditure tracking including breakdowns by cost centre, project, cost type and source of funds.


## Service Status

The Cloud Research Platform is currently in private beta.

## Contact
End-user support is provided through RONIN channels:

- [blog](https://blog.ronin.cloud/)
- [support](https://ronincloud.atlassian.net/servicedesk/customer/portals)
- [slack](https://blog.ronin.cloud/slack/)

For escalations and incidents, please use [cloudsupport@uis.cam.ac.uk](mailto:cloudsupport@uis.cam.ac.uk)

## Environments

We currently have one instance of RONIN running  at [https://uniofcam.cloud](https://uniofcam.cloud).

The service is co-managed with RONIN (company) which is responsible for the upgrades and monitoring of the service. 

## Operational documentation

Please visit [Operationl Documentation](https://gitlab.developers.cam.ac.uk/groups/uis/devops/ronin/-/wikis/home) internal wiki.

## Service Management and tech lead

The **service owner** for the service is  [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

The **service manager** for the service is (requested to the SMO, pending response)

The **tech lead** for the service is
[Andre Parmeggiani](https://www.lookup.cam.ac.uk/person/crsid/ap2249)

The following engineers have operational experience with the Cloud Research Platform and are able to
respond to support requests or incidents:

* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)

