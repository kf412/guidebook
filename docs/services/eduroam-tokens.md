title: Network Tokens

# Network Access Tokens Service

This page gives an overview of the Network Access Tokens service, describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Network Access Tokens site provides a site for Raven-authenticated users to obtain and manage per-device tokens allowing them to use eduroam and the University’s wireless network, and the VPN service.

## Service Status

The Network Access Tokens service is currently live.

## Contact

Technical queries and support should be directed to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access/-/issues).

End user support is provided by the [Service Desk](mailto:servicedesk@uis.cam.ac.uk).
## Environments

The Network Access Tokens service is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://tokens.uis.cam.ac.uk](https://tokens.uis.cam.ac.uk) | tokens-node1.csi.private.cam.ac.uk|
|             |                    | tokens-node2.csi.private.cam.ac.uk|
|             |                    | tokens-nodeq.csi.private.cam.ac.uk|

## Source code

The source code for the Network Access Tokens service is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access) | The source code for the main application server |

## Technologies used

The following gives an overview of the technologies the Network Access Tokens service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 10.5 |

## Operational documentation

The following gives an overview of how the Network Access Tokens service is deployed and maintained.
### How and where the Network Access Tokens service is deployed

The Network Access Tokens service application is deployed using RPM packages. These are built by Gitlab CI in the [Application Server repository](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access). These can be copied to the [Bes RPM package repository](https://bes.csi.cam.ac.uk) run by infra-sas@uis by following these [instructions](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#How_to_add_SLES_10.2F11_or_RHEL_.28S.29RPMS_to_Bes). As a last resort RPMs can be manually copied and installed.

### Deploying a new release

Once the RPM packages are added to the [Bes RPM package repository](https://bes.csi.cam.ac.uk) running `zypper in <package name>` will install, if the package is an upgrade, `zypper up` should upgrade the package(s).

### Monitoring

The Network Access Tokens service is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/).
There is also a [status page](https://tokens.uis.cam.ac.uk/adm/status) that checks the various components of the service and returns 200 if everything is functioning normally.

## Other documentation
* [End user documentation](https://help.uis.cam.ac.uk/service/network-services/tokens)
* [Eduraom WiFi](https://help.uis.cam.ac.uk/service/wi-fi)

## Service Management and tech lead

The **service owner** for the  Network Access Tokens service is [Robert Franklin](https://www.lookup.cam.ac.uk/person/crsid/rcf34).

The **service manager** for the Network Access Tokens service is [Rob Bricheno](https://www.lookup.cam.ac.uk/person/crsid/rwhb2).

The **tech lead** for the Network Access Tokens service is [Paul Badcock](https://www.lookup.cam.ac.uk/person/crsid/prb34).

The following engineers have operational experience with the Network Access Tokens service and are able to
respond to support requests or incidents:

* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
