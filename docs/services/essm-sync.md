title: ESSM Sync

# Education Space Scheduling and Modelling Sync

This page gives an overview of the Education Space Scheduling and Modelling synchronisation service,
describing its current status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

Student Registry procured a room scheduling application called [TermTime from Semestry](https://semestry.com/overview/).
Room Booking is currently handled by [Booker from EventMap](https://www.eventmapsolutions.com/osc-booker/).
A synchronisation process was required where the activities scheduled in TermTime would become
room bookings in Booker. Also, for consistency the room and building definitions in Booker would
need to be synchronised to TermTime.

## Service Status

The ESSM Sync is currently `alpha`.

Currently, which databases and discrepancies between Booker and TermTime are being ironed out.

Following this, a testing stage using a combination of Booker live and staging and a TermTime
training database, will confirm that the synchronised works as expected.

Finally, this will become live using a production TermTime database and Booker live.

## Contact

Technical queries and support should be directed to devops@uis.cam.ac.uk and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to devops@uis.cam.ac.uk rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
in the [administration project](https://gitlab.developers.cam.ac.uk/uis/devops/essm/administration/-/issues)
or [sync-tool repository](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tool/-/issues),
if a sync tool specific issue or you lack permission to the administration project.

## Environments

The ESSM Sync is currently deployed as Gitlab CI and Scheduled Pipelines in the Gitlab
[Synchronisation Tasks project](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tasks).

There is no additional infrastructure other than Gitlab itself and the DevOps Gitlab CI runners.

## Source code

The source code for the ESSM Sync is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Sync Tool](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tool) | The source code for the synchronisation processing |
| [Sync Tasks](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tasks) | The Gitlab CI and Terraform infrastructure code for creating the Scheduled Pipelines |
| [Administration](https://gitlab.developers.cam.ac.uk/uis/devops/essm/administration) | General project wide issue tracking and [documentation](https://gitlab.developers.cam.ac.uk/uis/devops/essm/administration/-/wikis/home) |


## Technologies used

The following gives an overview of the ESSM Sync is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Sync Tool | python | requests, PyYAML and dpath |
| Sync Tasks - Infrastructure | terraform | [gitlab provider](https://github.com/gitlabhq/terraform-provider-gitlab) |
| Sync Tasks - Gitlab CI | bash | docker |


## Operational documentation

The following gives an overview of how the ESSM Sync is deployed and maintained

### How and where the ESSM Sync is deployed

See [Sync Tasks](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tasks) Gitlab project.

### Deploying a new release

Updates to the [sync-tool](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tool) will be used
automatically by next schedule pipeline.

### Monitoring

Gitlab CI pipeline failures will cause automatic notification to the schedule owner (UIS DevOps Division Robot).

### Debugging

A local setup can be created by:

- installing the sync tool
```
pip install https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tool
```
- creating configuration files from the templates used in the
[Sync Tasks](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tasks) repository, containing
Booker and TermTime API credentials from ESSM 1password vault
- running `essmsync -c <configuration> <operation>` as described in
[sync tool](https://gitlab.developers.cam.ac.uk/uis/devops/essm/sync-tool) README

*Testing should be initially done against Booker staging instance and TermTime CambTest database.*

### Further Documentation

Detailed investigation of API usage and the synchronised processes may be found in the
[Administration Wiki](https://gitlab.developers.cam.ac.uk/uis/devops/essm/administration/-/wikis/home).


## Service Management and tech lead

The **service owner** is [Student Registry](https://www.student-registry.admin.cam.ac.uk/).

The **service manager** is [Neil King](https://www.lookup.cam.ac.uk/person/crsid/nik22).

The **tech lead** is [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21).

The following engineers have operational experience with the ESSM Sync service and are able to respond
to support requests or incidents:

* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23)
