title: GitLab

# GitLab service for the Developers' Hub

## Service Description
This page gives an overview of the GitLab service for the University
Developers' Hub.`, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! note
    The Developers' Hub is a University-wide project to foster a sense of
    community among software developers.

## Service Status

The GitLab service is currently live.

## Contact

Issues discovered in the service or new feature requests should be opened as GitLab issues
on the [support project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs).

## Environments

The GitLab service is currently deployed to the following environments:

The production GitLab service is available at
[gitlab.developers.cam.ac.uk](https://gitlab.developers.cam.ac.uk/).

Test environments are spun up on demand to test changes to the deployment. More
information is available in the [deployment project
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/deploy/blob/master/docs/bootstrap.md)
(DevOps only).

## Source code

The source code for the GitLab service is spread over the following repositories:

- [Deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy).

## Technologies used

The following gives an overview of the technologies the GitLab service is built on.

| Category | Language |
| -------- | -------- |
| Server | Ruby |
| Deployment | Helm |

## Operational documentation

The following gives an overview of how the GitLab service is deployed and maintained.
### Monitoring

Monitoring for GitLab is via StackDriver and is configured by the deploy terraform mostly from the [monitoring](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy/-/tree/master/environment/monitoring) module

### Debugging

Debugging is mostly done using the development instance of GitLab

### Deploying a new release

Deploying a new release, usually for dev/testing is documented in the [deploy](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy/-/blob/master/docs/workspaces.md) repo.

### User facing docs

- [User-facing
    documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/wikis/FAQs).
- [User support
    project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/)

## Service Management and tech lead

The **service owner** for the GitLab service is [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

The **service manager** for the GitLab service is [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The **tech lead** for the GitLab service is [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The following engineers have operational experience with the GitLab service and are able to
respond to support requests or incidents:

* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)
* [Andre Parmeggiani](https://www.lookup.cam.ac.uk/person/crsid/ap2249)