title: Google Workspace

# Google Workspace for Education @ Cambridge
_Formerly known as: GSuite@Cambridge_

All University members have access to a wide range of apps and services from Google Workspace, including (but not limited to):

  - Google Drive, including Shared Drives
  - Google Calendar
  - Google Chat
  - Google Meet
  - Google Sites
  - YouTube

_Access to some applications requires enabling via the [Google Workspace Preferences App](https://preferences.g.apps.cam.ac.uk/)._

## Service Description

The Google Workspace environment is supported by two components:

  1. Synchronisation of users (and groups) from [Lookup](../lookup/) to the Google directory
  2. Provision of SAML2 authentication via [Raven Core IdP](https://core-idp.raven.cam.ac.uk/)

The following describes the Google Workspace synchronisation component with the authentication
being covered by the [Raven OAuth2 service](../raven-oauth/).

## Service Status

The Google Workspace service is currently **live**.

## Contact

Technical queries and support should be directed to gapps-admin@uis.cam.ac.uk and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to gapps-admin@uis.cam.ac.uk rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues in the [sync tool repository](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/synctool/-/issues).

## Environments

The Google Workspace synchronisation is connected to the production Google Workspace for Education
at the `cam.ac.uk` domain. There is no test synchronisation but we have a test Google Workspace
at the `gdev.csi.cam.ac.uk` domain.

## Source code

The source code for the synchronisation process is spread over the following repositories:

| Repository  | Description        |
| ----------- | ------------------ |
| [Sync Tool](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/synctool) | The source code for synchronisation tool |
| [Lookup to GSuite Synchronisation Jobs](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy) | The GitLab schedule and CI to periodically perform the sync |

## Technologies used

The following gives an overview of the technologies that the synchronisation is built on:

| Category   | Language  | Framework(s)      |
| ---------- | --------- | ----------------- |
| Sync Tool  | Python    | Google Auth       |
|            |           | Google API client |
|            |           | LDAP3             |
| Scheduling | GitLab CI |                   |

## Operational documentation

Limited documentation can be found in the [Operational Docs wiki](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/operational-documentation/-/wikis/home)
(DevOps only).

The README.md files for the [sync tool](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/synctool/-/blob/master/README.md)
and [scheduling](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy/-/blob/master/README.md)
provide further documentation on how the tool and CI fit together.

End-user documentation can be found on the [UIS help site](https://help.uis.cam.ac.uk/service/collaboration/workspace).

### Monitoring

Pipeline successes and failures can be monitored in the scheduling repository's [CI Pipelines](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy/-/pipelines).

### Debugging

Testing is advised against the test Google Workspace at the `gdev.csi.cam.ac.uk` domain using the
[test sync-tool configuration file](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy/-/blob/master/test.yaml).

### Admin Console

Some administration actions (such as reporting and challenge lockout clearing) may be done via the
[Google Admin Console](https://admin.google.com/).

## Service Management and tech lead

The **service owner** for the Google Workspace is [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

The **service manager** for the Google Workspace is [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The **tech lead** for the Google Workspace is [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The following engineers have operational experience with the Google Workspace synchronisation and are able to
respond to support requests or incidents:

* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
