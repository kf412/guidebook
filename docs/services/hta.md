# Human Tissue Tracker

This page gives an overview of the Human Tissue Tracking application (HTA), describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Human Tissue Tracking application records information about Human Tissue stored within the University of Cambridge, and assists compliance with the Human Tissue Act.

## Service Status

The HTA is currently live.

## Contact

Technical queries and support should be directed to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/hta/hta/-/issues)

## Environments

The HTA is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://tissue.csx.cam.ac.uk/](https://tissue.csx.cam.ac.uk/) | hta-node1.csi.cam.ac.uk |
|             |                                                                | hta-node2.csi.cam.ac.uk |
|             |                                                                | hta-nodeq.csi.cam.ac.uk |
|             |                                                                | hta-live-db1.srv.uis.private.cam.ac.uk |
|             |                                                                | hta-live-db2.srv.uis.private.cam.ac.uk |
|             |                                                                | hta-live-db3.srv.uis.private.cam.ac.uk |
| Training | [https://training.tissue.csx.cam.ac.uk/](https://training.tissue.csx.cam.ac.uk/)        | hta-training.csi.cam.ac.uk |
|             |                                                                | devgroup-test-db.srv.uis.private.cam.ac.uk |

## Source code

The source code for the HTA is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hta/hta) | The source code for the main application server |


## Technologies used

The following gives an overview of the technologies the HTA is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 12.4


## Operational documentation

The following gives an overview of how the HTA is deployed and maintained.

### How and where the HTA is deployed

Deployed using RPM packages, built by gitlab CI and distributed by the [Bes RPM repository](https://bes.csi.cam.ac.uk/intro.html) see the [UCS Wiki page](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#How_to_add_SLES_10.2F11_or_RHEL_.28S.29RPMS_to_Bes) on adding RPMs to the Bes repository.

### Deploying a new release

Once the RPM packages are available via Bes `zypper up` can be run as root to deploy.

### Monitoring

The HTA database is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/).

There is also an [application status page](https://tissue.csx.cam.ac.uk/adm/status).

## Service Management and tech lead

The **service owner** for the HTA is currently unknown. See [the associated GitLab issue](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/-/issues/86)

The **service manager** for the HTA is [Helen Newport](https://www.lookup.cam.ac.uk/person/crsid/phn22).

The **tech lead** for the HTA is [Paul Badcock](https://www.lookup.cam.ac.uk/person/crsid/prb34)

The following engineers have operational experience with the HTA and are able to
respond to support requests or incidents:

* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23)
* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)

System Dependencies
-------------------
- [Raven](raven-ucamwebauth.md)
- [Moa](https://wiki.cam.ac.uk/ucs/Migrating_dev-group_databases_to_Moa)
