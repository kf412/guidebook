# Information Asset Register

This page gives an overview of the Information Asset Register, describing its current status, where
and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This service provides a web application for university institutions and colleges to register data
assets containing user details in accordance with GDPR guidelines.

## Service Status

The Information Asset Register is currently alpha.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/iar/webapp/-/issues).

## Environments

The Information Asset Register is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL | Backend API URL |
| ----------- | -------------------- | ---------------- | --------------- |
| Production  | [https://iar.admin.cam.ac.uk/](https://iar.admin.cam.ac.uk/) | [https://iar.admin.cam.ac.uk/admin](https://iar.admin.cam.ac.uk/admin) | [https://iar.admin.cam.ac.uk/api/](https://iar.admin.cam.ac.uk/api/) |
| Staging | [https://webapp-sdkppuphna-ew.a.run.app/](https://webapp-sdkppuphna-ew.a.run.app/) | [https://webapp-sdkppuphna-ew.a.run.app/admin](https://webapp-sdkppuphna-ew.a.run.app/admin) | [https://webapp-sdkppuphna-ew.a.run.app/api/](https://webapp-sdkppuphna-ew.a.run.app/api/) |
| Development | [https://webapp-5nblimpjjq-ew.a.run.app/](https://webapp-5nblimpjjq-ew.a.run.app/) | [https://webapp-5nblimpjjq-ew.a.run.app/admin](https://webapp-5nblimpjjq-ew.a.run.app/admin) | [https://webapp-5nblimpjjq-ew.a.run.app/api/](https://webapp-5nblimpjjq-ew.a.run.app/api/) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [GCP Cloud Run](https://console.cloud.google.com/run?project=iar-prod-039d468d) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=iar-prod-039d468d) |
| Staging | [GCP Cloud Run](https://console.cloud.google.com/run?project=iar-test-5ed587a9) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=iar-test-5ed587a9) |
| Development | [GCP Cloud Run](https://console.cloud.google.com/run?project=iar-devel-e1e48e71) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=iar-devel-e1e48e71) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=iar-meta-d66e0b2a).

## Source code

The source code for the Information Asset Register is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/iar/webapp) | The source code for the main application server |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iar/new-deploy) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Information Asset Register is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Web Application Backend | Python 3.7 | Django 2.2 |
| Web Application Frontend | JavaScript | React 16.2 |
| Web Application Frontend | JavaScript | Redux 3.7 |
| Database | PostgreSQL 11 | n/a |

## Operational documentation

The following gives an overview of how the Information Asset Register is deployed and maintained.

### How and where the Information Asset Register is deployed

The database for asset data is a PostgreSQL database hosted by GCP Cloud SQL. The main web
application is a Django backend with React frontend, hosted by GCP Cloud Run.

The Information Asset Register infrastucture is deployed using Terraform, with releases
of the main application and synchronisation job application deployed by the GitLab CD
pipelines associated with the
[infrastructure deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/iar/new-deploy).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to deploy the
Information Asset Register.

### Monitoring

The same method of monitoring the app is with Cloud Logs
- [production](https://console.cloud.google.com/logs?project=iar-prod-039d468d)
- [staging](https://console.cloud.google.com/logs?project=iar-test-5ed587a9)
- [development](https://console.cloud.google.com/logs?project=iar-devel-e1e48e71))

### Debugging

For debugging the deployed app see "Monitoring" above. For debugging locally the
[application `README.md`](https://gitlab.developers.cam.ac.uk/uis/devops/iar/webapp) describes
how the containerised app can be run.

## Service Management and tech lead

The **service owner** for the Information Asset Register is [James Knapton](https://www.lookup.cam.ac.uk/person/crsid/jak59).

The **service manager** for the Information Asset Register is [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

The **tech lead** for the Information Asset Register is [Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174).

The following engineers have operational experience with the Information Asset Register and are able to
respond to support requests or incidents:

* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
