# Jackdaw

This page gives an overview of the  service, describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Jackdaw provides a database that holds consolidated user administration records for staff and
students in the University. Jackdaw get daily feeds from CamSIS, CHRIS, the Card Office and
daily feeds out to a variety of services/systems.

## Service Status

Jackdaw is currently live.

## Contact

Technical queries and support should be directed to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)
and will be picked up by a member of the team working on the service. To ensure that you receive a
response, always direct requests to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/jackdaw-projects/jackdaw/-/issues)
(DevOps and Service Manager only).

## Environments

Jackdaw is currently deployed to the following environment:

| Name        | URL                      | Supporting VMs  |
| ----------- | ------------------------ | --------------- |
| Production  | https:/jackdaw.cam.ac.uk | skua.csi.cam.ac.uk |
|             |                          | owl.csi.cam.ac.uk (standby) |
|             |                          | jay.csi.cam.ac.uk (standby) |
|             |                          | ruff.csi.cam.ac.uk (test)   |

Different combinations of database, web server and pages are accessed via urls
of the format: `https://jackdaw.cam.ac.uk[:PORT]/~/[DatabaseName]/[TypeofPage]`

e.g. https://jackdaw.cam.ac.uk[:443,:8443]/~/[jackdaw,jdawdev.jdawtest,jdawtmp]/[t,-]

Specifically:

- https://jackdaw.cam.ac.uk/ (same as https://jackdaw.cam.ac.uk:443/~/jackdaw/-)
    - Production server backed by production database and production pages
- https://jackdaw.cam.ac.uk/t (same as https://jackdaw.cam.ac.uk:443/~/jackdaw/t)
    - Production server backed by production database test pages
- https://jackdaw.cam.ac.uk:8443/
    - Test server backed by production database (should be used with caution)
- https://jackdaw.cam.ac.uk/~/[jdawdev,jdawtest,jdawtmp]
    - Production server backed by develop/test/experiment DB - harmless good for what it is
- https://jackdaw.cam.ac.uk:8443/~/[jdawdev,jdawtest,jdawtmp]/t
    - Good for testing DB patches, httpd config and test pages

## Source code

The source code for future Jackdaw development is under the
https://gitlab.developers.cam.ac.uk/uis/devops/iam/jackdaw-projects group.

Multiple RCS directories on the production server replicated to standby servers
using bespoke Replication Manager.

## Technologies used

The following gives an overview of the technologies Jackdaw is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Server   | Perl/v5.18.2 PerlToolKit PLSQL | Apache/2.2.34 (Unix)  mod_perl/2.0.8  |
| Client | Jquery | |

## Operational and other documentation

* [Jackdaw on Confluence](https://confluence.uis.cam.ac.uk/display/JAC/Jackdaw+Home)
* [Jackdaw - service definition](https://confluence.uis.cam.ac.uk/display/IAM/Jackdaw+-+service+definition)
* [Jackdaw UIS JIRA](https://jira.uis.cam.ac.uk/secure/Dashboard.jspa?selectPageId=11002)
* [Jackdaw HPC JIRA](https://jira.hpc.cam.ac.uk/secure/Dashboard.jspa?selectPageId=10500)
* [UIS help site documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup)
* [Web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws)

## Service Management and tech lead

The **service owner** for Jackdaw is [Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

The **service manager** for Jackdaw is [Dr Ujjwal Das](https://www.lookup.cam.ac.uk/person/crsid/ukd20)

The following DevOps engineers have access to the Jackdaw service:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)
* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
