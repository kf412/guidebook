# Legacy Card System

This page gives and overview of the Legacy Card System, describing it's current status, where and
how it's developed and deployed, and who is responsible for maintaining it.


!!! danger
    The Legacy Card System uses technology, coding standards, and deployment methodologies which
    are not endorsed by the TDA and not standard practice for the Devops team.

## Service Description

The Legacy Card System allows for the management of University Cards, providing functionality
to track cards and cardholders, integrate with card printers, and export card data to other systems.

## Service Status

The Legacy Card System is currently __being decommissioned__ and replaced by a new card system
[currently under development](https://www.uis.cam.ac.uk/it-portfolios/projects/card-system-replacement).

In the interim the Legacy Card System system is being supported and maintained, but no new
development work is being completed and only high priority bug fixes will be made.

## Contact

Technical queries and support should be directed to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk)
and will be picked up by a member of the team working on the service. To ensure that you receive a
response, always direct requests to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/feedback/-/issues)

## Environments

The Legacy Card System is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | https://webservices.admin.cam.ac.uk/uc/ | `sanders.internal.admin.cam.ac.uk` |
| Staging     | http://staging.app3.admin.cam.ac.uk/uc/ | `jackson.internal.admin.cam.ac.uk` |
| Development | http://staging.app4.admin.cam.ac.uk/uc/ | `rich.internal.admin.cam.ac.uk` |

All VMs are maintained by infra-sas and also host additional applications. SSH access to the VMs
can be gained by [emailing infra-sas](mailto:uisinfrastructureserversandstorage@uis.cam.ac.uk).

All applications are run by the `httpd` user, which can be `su`'d to within each VM, the password
for this user is held in the Devops 1Password instance.

## Database environments

The Legacy Card System holds a large amount of its logic within stored procedures and functions
contained within the Oracle database. The following Oracle databases exist which serve the
Legacy Card System:

| Name        | Serves environment  | DB Name  |
| ----------- | ------------------- | -------- |
| Production  | Production, Staging | CARD     |
| Dev         | Development         | CARD_T   |

Credentials for accessing both databases can be found within the Devops team's 1Password instance.

The database servers are maintained by the [DBA Team](mailto:uisdba@admin.cam.ac.uk) and are
accessible through the OCM.

## Source code

The source code for the Legacy Card System is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Webapp Server](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-system) | The source code for the 'uc' application server |
| [Webservices](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/legacy-card-webservices) | The source code for the card 'webservices' providing ad-hoc HTTP endpoints |

!!! danger
    Both these repositories contain secrets and passwords which are used by production instances
    of the service, access to them should not be granted to anyone outside UIS.


## Technologies used

The following gives an overview of the Legacy Card System is built on.

| Category | Language    | Framework(s) |
| -------- | --------    | --------- |
| Client   | JavaScript  | Some JQuery |
| Server   | Perl        | [DBI](https://metacpan.org/pod/DBI) for database access |
| Database | Oracle 19c  | N/A |

## Operational documentation

The following gives an overview of how the Legacy Card System is deployed and maintained.

!!! danger
    Deployments to the Legacy Card System are manual and uncontrolled, therefore a deployment
    should only be made where absolutely necessary, i.e. a high priority bug fix.

### How and where the Legacy Card System is deployed

The process which has been followed to make a change to **application (Perl)** code is:

* Apply the change to the development environment (`rich.internal.admin.cam.ac.uk`)
* Test that the change works as expected
* Copy the changes made into [source control](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-system/)
* Raise a merge request containing the change and ensure the change is reviewed and merged
* Copy the change on the live system (`sanders.internal.admin.cam.ac.uk`)
* Smoke test the change on live

Previously a script has been run to sync the live system from the development system
([as documented here](https://confluence.uis.cam.ac.uk/display/UC/Releasing+to+Live)). But the
code deployed to the development system has drifted from what exists in live, and so instead
the approach of cherry-picking changes between source control and the dev and live system has
been used.

The process which has been followed to make a change to **stored proceedures** code is:

* [Install and configure SQL Developer](https://www.oracle.com/uk/database/technologies/appdev/sqldeveloper-landing.html)
    * On modern Macs there seems to be no way around doing this
    * On Linux it's possible that [the approach documented here may work](https://medium.com/@webchad/run-oracles-sql-developer-with-docker-on-mac-d092c4b9ea78)
* Connect to the `CARD_T` database, make the required change and test
* Copy the changes made into [source control](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-system/)
* Raise a merge request containing the change and ensure the change is reviewed and merged
* Connect to the `CARD` database and apply the same change
* Smoke test the change on live

This broadly follows the approach [documented here](https://confluence.uis.cam.ac.uk/display/UC/Releasing+PLSQL+to+live),
except we keep
[the card system repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-system/)
as the source of truth for changes to stored procedures / database functions.

### Monitoring

!!! danger
    There is no automated monitoring or alerting of the Legacy Card System. Where problems have
    occurred previously, the development team has been alerted by card office or card representative
    staff.

Logs from the application (Perl) scripts can be found within `ACTIVITYLOG`, `ERRORLOG` and `DEBUGLOG`
in `/home/httpd/html/uc`. Logs from database processes are scp'd nightly from the database
server and can be found in `/tmp/card_process_logs/process_logs`.

### Debugging

The Legacy Card System cannot be run locally, and therefore the development instance should be
used to trial changes and fixes. The logs detailed under `Monitoring` are the best way to diagnose
problems with the system.

### Feeds of data into the Legacy Card System

The list of feeds of data into and out of the card system
[are documented here](https://docs.google.com/spreadsheets/d/1Nfeyhn5ZABu5msvZDulj6_2YODLOaoXw/edit#gid=20680651).
Additionally a write-up has been completed documenting
[how data is fed into the Legacy Card System](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation/-/wikis/card/card-system-data-feeds).

## Additional documentation

Further documentation about the Legacy Card System can be found:

* [In the University Card space on Confluence](https://confluence.uis.cam.ac.uk/display/UC/University+Card+Home)
    * This documentation contains a range of operational documentation and information about previous projects
* [In the Service Desk Knowledge Base](https://wiki.cam.ac.uk/uis/Service_Desk_Knowledgebase:_University_Card#University_Card_Entitlement)
    * This documentation is mainly focused on front-line issues and card printing
* [In the Wiki for the new Card Management System project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation/-/wikis/card/Architecture)
     * The documentation includes an overview of the Legacy Card System and its pitfalls

## Service Management and tech lead

The **service owner** for the Legacy Card System is **currently vacant**.

The **service manager** for the Legacy Card System is **currently vacant**.

The **tech lead** for the Legacy Card System is [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23).

The following engineers have operational experience with the Legacy Card System and are able to
respond to support requests or incidents:

* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)
* [Stephen Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
