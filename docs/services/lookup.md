# Lookup

This page gives an overview of the Lookup service, describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Lookup provides an [directory of University staff, institutions and groups](https://www.lookup.cam.ac.uk/) available via
[LDAP](https://help.uis.cam.ac.uk/service/collaboration/lookup/ldapqueries/ldapqueries) (Uni only) or a [web API](https://www.lookup.cam.ac.uk/doc/ws-doc/).

## Service Status

Lookup is currently live.

## Contact

Technical queries and support should be directed to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always direct requests to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team members directly.

Queries regarding correctness or completeness of data should be sent to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis/-/issues).

## Environments

Lookup is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | https://www.lookup.cam.ac.uk | dingo.csi.cam.ac.uk |
|             |                              | wolf.csi.cam.ac.uk  |
|             | ldap{1,2}.lookup.cam.ac.uk (HA LDAP endpoints)  | dingo.csi.cam.ac.uk |
|             |                              | wolf.csi.cam.ac.uk  |
|             | [https://ldap.lookup.cam.ac.uk](https://ldap.lookup.cam.ac.uk) (live LDAP endpoint) | dingo.csi.cam.ac.uk |
|             | lookup-live-db.srv.uis.private.cam.ac.uk | lookup-live-db.srv.uis.private.cam.ac.uk |
|             |                              | wolf.csi.cam.ac.uk  |
| Staging     | https://lookup-test.csx.cam.ac.uk    | jodrell.csx.cam.ac.uk |
|             | lookup-test.csx.cam.ac.uk (test LDAP endpoint)   | jodrell.csx.cam.ac.uk |
| New RHEL    | N/A | lookup-live1.srv.uis.private.cam.ac.uk |
| VMs         |     | lookup-live2.srv.uis.private.cam.ac.uk |

## Source code

The source code for Lookup is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis) | The source code for the main application server |
| [Lookup API Client](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis-client) | API Client |

## Technologies used

The following gives an overview of the technologies Lookup is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Java + Groovy| Grails 1.3.10 |
| DB     | Postgres      | 12.4 |

## Operational documentation

The following gives an overview of how Lookup is deployed and maintained.

### How and where Lookup is deployed

Deployed using RPM packages from the [Bes RPM repository](https://bes.csi.cam.ac.uk/yum/).

A [video recording of a deployment](https://drive.google.com/file/d/1JL2qWBfRDXOtJ9gl7tTw4XkU2NAZwpCk/view) is available (DevOps only).

### Deploying a new release

RPM packages are built by [gitlab CI](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis/-/blob/master/.gitlab-ci.yml).

### Other documentation

- [UIS help site documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup)
- [Web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws)
### Monitoring

Lookup is monitored by [nagios](https://nagios.uis.cam.ac.uk):

* [Overview](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup&style=overview)
* [Disc Space](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-disc-space&style=overview)
* [SSH Server](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-ssh-server&style=overview)
* [SSL Certs](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-ssl-certs&style=overview)
* [Lookup Web Application](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-webapp&style=overview)

### Debugging

 The [Lookup server readme](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis) contains details on running a local copy for debugging.

## Service Management and tech lead

The service owner for Lookup is currently unknown. See the [associated GitLab issue](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/-/issues/89).

The **service manager** for Lookup is [Currently vacant](https://www-falcon.csx.cam.ac.uk/site/UISINTRA/how/itsm/service-management-list).

The **tech lead** for Lookup is [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23).

The following engineers have operational experience with Lookup and are able to respond to support requests or incidents:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)
* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
