# Password Management

This page gives an overview of the Raven Password Management service, describing
its current status, where and how it's developed and deployed, and who is
responsible for maintaining it.

## Service Description

This services allows Raven account holders to manage their password. Changing of
passwords can be performed via 'normal' login, self-service recovery or password
reset tokens. These tokens may be issued by departmental or college
administrators, or by University Information Services.

Self-service password recovery allows users to reset their password by
configuring a recovery email address and/or mobile phone number and set up some
security questions that will be used to prove their identity during
password recovery.

Changed passwords are synchronised with a wide range of University systems,
including Raven based websites, Exchange Online email, Hermes email and Desktop
Services including the Managed Cluster Service (MCS).

A history of changes to passwords and recovery details is also visible to an
authenticated user.

## Service Status

The Raven Password Management service is currently **live**.

The intention is to move this service from UIS infrastructure VMs to a Google
Cloud deployment but no timeline for this is currently set.

## Contact

End-user support is provided by the [Service Desk](mailto:servicedesk@uis.cam.ac.uk)
and [User Admin](mailto:user-admin@uis.cam.ac.uk).

Technical queries and support should be directed to the
[Service Desk](mailto:servicedesk@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a
response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues [here](https://gitlab.developers.cam.ac.uk/uis/devops/passwords/passwords/-/issues).

## Environments

The Password app is currently deployed to the following environments:

| Name            | URL                                           | Supporting VMs                                   |
| --------------- | --------------------------------------------- | ------------------------------------------------ |
| Production.     | https://password.raven.cam.ac.uk/             | password-node1.csi.private.cam.ac.uk (primary)   |
|                 |                                               | password-node2.csi.private.cam.ac.uk (secondary) |
|                 |                                               | password-nodeq.csi.private.cam.ac.uk (quorum)    |
| Test            | https://password-test.csx.cam.ac.uk/          | password-test.csx.cam.ac.uk                      |
|                 |                                               | devgroup-test-db.srv.uis.private.cam.ac.uk       |
| New VMs (RHEL8) | UAT: https://uis-tm.password.raven.cam.ac.uk/ | raven-password-live1.srv.uis.private.cam.ac.uk   |
|                 |                                               | raven-password-live2.srv.uis.private.cam.ac.uk   |

> password-test.csx has its own fake local password clients that act as black
> holes for all password changes. Any actions that require a password to be
> quoted on password-test.csx (not including the initial Raven logon) should
> quote the fake password stored in 1password.

## Source code

The source code for the Password Management Application is stored in
[this Gitlab repository](https://gitlab.developers.cam.ac.uk/uis/devops/raven/passwords/passwords)

> password-test.csx is updated nightly from git master.

## Technologies used

The following gives an overview of the technologies the Password app is built on.

| Category | Language    | Framework(s) |
| -------- | ----------- | ------------ |
| Server   | Java+Groovy | Grails       |
| DB       | Postgres    | [Moa](https://wiki.cam.ac.uk/ucs/Migrating_dev-group_databases_to_Moa) |

## Operational documentation

The following gives an overview of how the Password app is deployed and maintained.

### Deploying a new release

Deployed by installing RPMs from the [Bes repo](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#.28S.29RPMS_Available_on_Bes)

### Monitoring

The Password app is monitored by the UIS infra-sas
[nagios service](https://nagios.uis.cam.ac.uk/nagios/).

Sevices currently monitored:

* ping - standard nagios ping check.
* SSL - checks for a valid TLS certificate on port 8443.
* https_devgroup - checks for a 200 response from the /adm/status page on port 8443.
* disc-space - checks for at least 15% free disk space.

There is also a check for a vaild TLS certificate being served by the traffic manager for https://uis-tm.password.raven.cam.ac.uk

### Debugging

The Password app cannot be run locally, and therefore the test instance should
be used to trial changes and fixes.

### Other operational documentation

End-user documentation: https://help.uis.cam.ac.uk/service/accounts-passwords

Description of the password strength checker: https://wiki.cam.ac.uk/uis/UIS_Password_Strength_Checker

The self-service password recovery flow: https://gitlab.developers.cam.ac.uk/uis/devops/raven/passwords/passwords/-/blob/master/doc/password-recovery.pdf

## Service Management and tech lead

The **service owner** for the Password app is [Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

The **service manager** for the Password app is [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)

The **tech lead** for the Password app is [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)

The following engineers have operational experience with the Password app and are able to respond to support requests or incidents:

* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23)
* [Paul Badcock](https://www.lookup.cam.ac.uk/person/crsid/prb34)
