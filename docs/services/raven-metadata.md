title: Metadata

# Shibboleth Metadata Administration service

This page gives an overview of the Shibboleth Metadata Administration service (Metadata App),
describing its current status, where and how it's developed and deployed, and who is responsible
for maintaining it.

## Service Description

This service allows web site administrators in the University to register web sites so that they
work with the shibboleth provided by the [Raven SAML2 service](../raven-shibboleth/) and
subsequently to manage those registrations. This is achieved by uploading 'SAML Metadata'
describing those sites.

## Service Status

The Metadata App is currently live.

## Contact

Technical queries and support should be directed to servicedesk@uis.cam.ac.uk and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to servicedesk@uis.cam.ac.uk rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues [here](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata/-/issues).

## Environments

The Metadata App is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL |
| ----------- | -------------------- | ---------------- |
| Production  | [https://metadata.raven.cam.ac.uk/](https://metadata.raven.cam.ac.uk/) | [https://metadata.raven.cam.ac.uk/admin](https://metadata.raven.cam.ac.uk/admin) |
| Staging | [https://webapp.test.shib-metadata.gcp.uis.cam.ac.uk/](https://webapp.test.shib-metadata.gcp.uis.cam.ac.uk/) | [https://webapp.test.shib-metadata.gcp.uis.cam.ac.uk/admin](https://webapp.test.shib-metadata.gcp.uis.cam.ac.uk/admin) |
| Development | [https://webapp.devel.shib-metadata.gcp.uis.cam.ac.uk/](https://webapp.devel.shib-metadata.gcp.uis.cam.ac.uk/) | [https://webapp.devel.shib-metadata.gcp.uis.cam.ac.uk/admin](https://webapp.devel.shib-metadata.gcp.uis.cam.ac.uk/admin) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [GCP Cloud Run](https://console.cloud.google.com/run?project=shib-metadata-prod-8adc3a8f) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=shib-metadata-prod-8adc3a8f) |
| Staging | [GCP Cloud Run](https://console.cloud.google.com/run?project=shib-metadata-test-3d1b697a) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=shib-metadata-test-3d1b697a) |
| Development | [GCP Cloud Run](https://console.cloud.google.com/run?project=shib-metadata-devel-d10d66c7) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=shib-metadata-devel-d10d66c7) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=shib-metadata-meta-0f6f7257).

## Source code

The source code for the Metadata App is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata) | The source code for the main application server |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata-deployment) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Metadata App is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Web Application | Python 3.8 | Django 2.2 |
| Database | PostgreSQL 11 | n/a |

## Operational documentation

The following gives an overview of how the Metadata App is deployed and maintained.

### How and where the Metadata App is deployed

The database for metadata is a PostgreSQL database hosted by GCP Cloud SQL. The main web
application is a classic Django application (not DRF), hosted by GCP Cloud Run.

The Metadata App infrastructure is deployed using Terraform, with releases
of the main application application deployed by the GitLab CD pipelines associated with the
[infrastructure deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata-deployment).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to deploy the
Metadata App.

### Monitoring

The same method of monitoring the app is with Cloud Logs

- [production](https://console.cloud.google.com/logs?project=shib-metadata-prod-8adc3a8f)
- [staging](https://console.cloud.google.com/logs?project=shib-metadata-test-3d1b697a)
- [development](https://console.cloud.google.com/logs?project=shib-metadata-devel-d10d66c7)

### Debugging

For debugging the deployed app see "Monitoring" above. For debugging locally the
[application `README.md`](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shibboleth-metadata)
describes how the containerised app can be run.

### Other operational documentation

- [Generic Raven SAML 2.0 instructions](https://docs.raven.cam.ac.uk/en/latest/raven-saml2/)
- [Legacy documentation](https://wiki.cam.ac.uk/raven/Shibboleth_documentation_and_HOWTOs)

## Service Management and tech lead

The **service owner** for the Metadata App is [Vijay
Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1).

The **service manager** for the Metadata App is [Rich
Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The **tech lead** for the Metadata App is [Rich
Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjg21).

The following engineers have operational experience with the Metadata App and are able to
respond to support requests or incidents:

* [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)
