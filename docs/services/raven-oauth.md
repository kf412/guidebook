title: OAuth2

# Raven OAuth2

Raven is the web authentication service for the University. This page documents
the OAuth2 personality for the Raven service.

Environments and Servers they run on
------------------------------------

- [Production]({link to application}) {link to api if applicable (see information-asset-register.md)}
    - {list the servers the application is deployed to (see self-service-gateway.md) or link to cluster management (see information-asset-register.md)}
- [Test]({link to application}) {include link to api if applicable see information-asset-register.md}
    - {list the servers the application is deployed to (see self-service-gateway.md) or link to cluster management (see information-asset-register.md)}
- {any other environments}

Application repositories
------------------------
- [{application component 1}]({link to repo})
- [{application component 2}]({link to repo})

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | {name and version} | {name and version} |
| Client | {name and version} | {name and version} |
| {other} | {name and version} | {name and version} |

Deployment
----------
{brief description of technologies involved - links where appropriate (use completed templates as guide)}

Deployment repository
---------------------
- [{application deployment repo 1}]({link to repo})
- [{application deployment repo 2}]({link to repo})

Service Owner
-------------
[{Full Name}](https://www.lookup.cam.ac.uk/person/crsid/{crsid})

Service Managers
----------------
[{Full Name}](https://www.lookup.cam.ac.uk/person/crsid/{crsid})

Current Status
--------------
Alpha

Documentation
-------------
- [{description of documentation 1}]({link to documentation})
- [{description of documentation 2}]({link to documentation})
