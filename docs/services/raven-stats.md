title: Statistics API

# Raven Device Statistics API

This page gives an overview of the Raven Device Statistics API, describing its
current status, where and how it's developed and deployed, and who is
responsible for maintaining it.

## Service Description

The [Raven Device Statistics
API](https://developer.api.apps.cam.ac.uk/docs/ravenstats/1/overview) provides
statistics on unique visits recorded on raven.cam.ac.uk over one or more
reporting periods.

The API is useful for getting metrics on devices currently used by University
members for planning device support.

Devices are grouped by web browser make and operating system.

Reporting periods are the past day, past week and past 30 days.

## Service Status

The Raven Device Statistics API is currently in alpha as part of the wider alpha
testing for the API Gateway service.

## Contact

Technical queries and support should be directed to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) and will be
picked up by a member of the team working on the service. To ensure that you
receive a response, always direct requests to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues in the [infrastructure
project](https://gitlab.developers.cam.ac.uk/uis/devops/raven/infrastructure)
(DevOps only). Non DevOps members may open issues in that project by emailing
[gitlab+uis-devops-raven-infrastructure-1920-issue-@developers.cam.ac.uk](mailto:gitlab+uis-devops-raven-infrastructure-1920-issue-@developers.cam.ac.uk).

## Environments

The Raven Device Statistics API is currently deployed to the following environments:

| Name        | URL                |
| ----------- | ------------------ |
| Production  | https://api.apps.cam.ac.uk/ravenstats/ |
| Staging  | https://api.apps.cam.ac.uk/ravenstats-test/ |
| Development  | https://api.apps.cam.ac.uk/ravenstats-dev/ |

The production environment is advertised on the [API Gateway developer
portal](https://developer.api.apps.cam.ac.uk/). Other environments are not
publicised.

## Source code

The source code for the Raven Device Statistics API is present as a single
Python script within the [Raven infrastructure
project](https://gitlab.developers.cam.ac.uk/uis/devops/raven/infrastructure)
(DevOps only). The script is run as a Cloud Scheduled Job each hour to update a
static device report in a Cloud Storage object.

## Technologies used

The Raven stats API is built on the following technologies:

* Google Cloud Storage to provide the live "implementation" of the API backend.
* Google Cloud Functions to run periodic reports.
* Google Cloud Scheduler to trigger periodic reports.
* Google Analytics to provide device detection.
* The API Gateway to publish the API.

A lot of that technology is hidden within our [standard scheduled
script module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script).

## Operational documentation

The following gives an overview of how the Raven Device Statistics API is
deployed and maintained.

### How and where the Raven Device Statistics API is deployed

The Raven Device Statistics API is deployed as both a backend proxied by the API
Gateway and a product within the API Gateway itself.

The backend is a Google Storage object which is provisioned with terraform. The
content of that object is rendered by a scheduled script.

The proxy is defined in the [API Gateway
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/api/gateway-ops/-/blob/master/services.tf).

### Deploying a new release

A new release can be deployed by applying the Raven infrastructure terraform.

### Monitoring

The GCP scheduled script terraform module sets up a Cloud Monitoring alert which
is fired if the script does not complete successfully sufficiently frequently.

### Debugging

If the scheduled script is failing, use the GCP console to look at the logs of
the Raven stats Cloud Function in the first instance.

## Service Management and tech lead

The **service owner** for the Raven Device Statistics API is unknown pending
  discussion into service management for APIs.

The **service manager** for the Raven Device Statistics API is
  [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The **tech lead** for the Raven Device Statistics API is
  [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The following engineers have operational experience with the Raven Device Statistics API and are able to
respond to support requests or incidents:

* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23).
