# UCamWebAuth

Raven is the web authentication service for the University. This page documents
the UCamWebAuth personality for the Raven service.

Environments
------------

- [Production](https://raven.cam.ac.uk/) - authenticates users by CRSid.

Application repositories
------------------------

- [Web application](https://gitlab.developers.cam.ac.uk/uis/devops/raven/webauth) (DevOps only)

The individual VMs hosting the service are
[listed](https://gitlab.developers.cam.ac.uk/uis/devops/raven/operational-documentation/wikis/raven/List-of-VMs)
in the Operational Documentation. (DevOps only)

Technology
----------

!!! warning
    This service uses a technology stack which does not align with DevOps
    Division recommendations.

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Perl | Mason |

Deployment
----------

Deployment is via Ansible playbook to an on-premise set of servers. See the
[operational
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/raven/operational-documentation)
project (DevOps only) for more information.

Deployment repository
---------------------

- [Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/raven/ansible-raven) (DevOps only)

Service Owner
-------------
[Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

Service Managers
----------------
[Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)

Current Status
--------------

Live

Documentation
-------------

!!! note
    Raven documentation is in the process of being rationalised into a single
    place.

- [Raven wiki](https://wiki.cam.ac.uk/raven/Main_Page)
- [Project page](https://raven.cam.ac.uk/project/)
- [UCS Wiki pages](https://wiki.cam.ac.uk/ucs/Raven) (UIS only)
