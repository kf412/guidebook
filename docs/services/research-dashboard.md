# Research Dashboard

This page gives an overview of the Research Dashboard, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This service allows those involved in research administration to view:

- Grant finances (summary expenditure, grant balances and team details)
- Application deadlines
- Contract status and progress updates

## Service Status

The Research Dashboard is currently beta.

The Research Dashboard service is planned to be migrated to a more standard
[GCP Cloud Run deployment](https://gitlab.developers.cam.ac.uk/groups/uis/devops/research-dashboard/-/epics/13).

## Contact

Technical queries and support should be directed to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp/-/issues).

## Environments

The Self-Service Gateway is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL | Backend API URL |
| ----------- | -------------------- | ---------------- | --------------- |
| Production  | [https://researchdashboard.admin.cam.ac.uk/](https://researchdashboard.admin.cam.ac.uk/) | [https://researchdashboard.admin.cam.ac.uk/admin](https://researchdashboard.admin.cam.ac.uk/admin) | [https://researchdashboard.admin.cam.ac.uk/api/](https://researchdashboard.admin.cam.ac.uk/api/) |
| Staging  | [https://test.researchdashboard.admin.cam.ac.uk/](https://test.researchdashboard.admin.cam.ac.uk/) | [https://test.researchdashboard.admin.cam.ac.uk/admin](https://test.researchdashboard.admin.cam.ac.uk/admin) | [https://test.researchdashboard.admin.cam.ac.uk/api/](https://test.researchdashboard.admin.cam.ac.uk/api/) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [Kubernetes Engine (Namespace: pidash-production)](https://console.cloud.google.com/kubernetes/clusters/details/europe-west2/cluster/details?project=uis-automation-pidash) | [GCP Cloud SQL (DB name: pidash-production)](https://console.cloud.google.com/sql/instances/db/overview?project=uis-automation-pidash) |
| Staging | [Kubernetes Engine (Namespace: pidash-test)](https://console.cloud.google.com/kubernetes/clusters/details/europe-west2/cluster/details?project=uis-automation-pidash) | [GCP Cloud SQL (DB name: pidash-test)](https://console.cloud.google.com/sql/instances/db/overview?project=uis-automation-pidash) |

## Source code

The source code for the Research Dashboard is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp) | The source code for the main application server. |
| [CUFS GMS Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/datamart-api) | The source code for the micro-service querying CUFS grant management system data. |
| [CHRIS Grant Contracts Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/chris-salaries-api) | The source code for the micro-service querying CHRIS contracts data. |
| [CUFS Salary Commitments Calculator](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/salaries-commitments) | The source code for a salaries commitments calculator for CUFS. |
| [Staff oncost calculator](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/ucamstaffoncosts) | The source code for a python module to calculate total staff oncosts. |
| [School Data Cache](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/school-data-cache) | The source code for caching of university school metadata. |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/deploy) | The Google Cloud Deployment Manager infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Research Dashboard is built on.

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python 3.7 | Django 2.2 |
| Client | JavaScript | jQuery v3.2.1 |
| Client | TypeScript | React 16.12 |

## Operational documentation

The following gives an overview of how the Research Dashboard is deployed and maintained.

### How and where the Self-Service Gateway is deployed

Database for application data is a PostgreSQL database hosted by GCP Cloud SQL.
The main web application is a Django application, hosted by a GCP Kubernetes cluster.

### Deploying a new release

The README.md file in the Infrastructure Deployment repository explains how to deploy the Research Dashboard.

### Monitoring

The application emails errors to `automation+pidash-production@uis.cam.ac.uk`. More detailed logs can be found on the
[GCP Console](https://console.cloud.google.com/logs/viewer?project=uis-automation-pidash).

### Debugging

See monitoring.

### Administration

Any operational or administration issues should be raised as an issue in [a repository for this purpose](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/operations/-/issues).

## Service Management and tech lead

The **service owner** for the Research Dashboard is [Dawn Edwards](https://www.lookup.cam.ac.uk/person/crsid/dgb26).

The **service managers** for the Research Dashboard are
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203) and
[Will Russell](https://www.lookup.cam.ac.uk/person/crsid/weer2).

The **tech lead** for the Research Dashboard is [Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174).

The following engineers have operational experience with the Research Dashboard and are able to
respond to support requests or incidents:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
