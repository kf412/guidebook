# Subject Moderation Interface (SMI)

This page gives an overview of the Subject Moderation Interface (SMI), describing its current
status, where and how it's developed and deployed, and who is responsible for maintaining it.

!!! warning
    This is a **prototype** service that is not fully supported. See the
    [FAQ for Subject Moderation Interface alpha](https://docs.google.com/document/d/1d-wrCkJFgSL3Z6G12ze2F2Szjvou-hFGdU3pJ24MzTQ)
    for more details.

## Service Description

The Subject Moderation Interface (SMI) service provides a web application for moderating
undergraduate applications as part of the admissions process.

## Service Status

The SMI is currently alpha.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/uga/support).

## Environments

The SMI is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL | Backend API URL |
| ----------- | -------------------- | ---------------- | --------------- |
| Production  | [https://alpha.subjectmoderationinterface.apps.cam.ac.uk/](https://alpha.subjectmoderationinterface.apps.cam.ac.uk/) | [https://alpha.subjectmoderationinterface.apps.cam.ac.uk/admin](https://alpha.subjectmoderationinterface.apps.cam.ac.uk/admin) | [https://alpha.subjectmoderationinterface.apps.cam.ac.uk/api/](https://alpha.subjectmoderationinterface.apps.cam.ac.uk/api/) |
| Staging     | [https://staging.subjectmoderationinterface.apps.cam.ac.uk/](https://staging.subjectmoderationinterface.apps.cam.ac.uk/) | [https://staging.subjectmoderationinterface.apps.cam.ac.uk/admin](https://staging.subjectmoderationinterface.apps.cam.ac.uk/admin) | [https://staging.subjectmoderationinterface.apps.cam.ac.uk/api/](https://staging.subjectmoderationinterface.apps.cam.ac.uk/api/) |
| Development | [https://webapp.devel.uga.gcp.uis.cam.ac.uk/](https://webapp.devel.uga.gcp.uis.cam.ac.uk/) | [https://webapp.devel.uga.gcp.uis.cam.ac.uk/admin](https://webapp.devel.uga.gcp.uis.cam.ac.uk/admin) | [https://webapp.devel.uga.gcp.uis.cam.ac.uk/api/](https://webapp.devel.uga.gcp.uis.cam.ac.uk/api/) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database | Synchronisation Job Application Hosting |
| ----------- | ------------------------ | -------- | --------------------------------------- |
| Production  | [GCP Cloud Run](https://console.cloud.google.com/run?project=uga-prod-4ef210b2) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=uga-prod-4ef210b2) | [GCP Cloud Scheduler](https://console.cloud.google.com/cloudscheduler?project=uga-prod-4ef210b2) |
| Staging     | [GCP Cloud Run](https://console.cloud.google.com/run?project=uga-test-1f58c76d) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=uga-test-1f58c76d) | [GCP Cloud Scheduler](https://console.cloud.google.com/cloudscheduler?project=uga-test-1f58c76d) |
| Development | [GCP Cloud Run](https://console.cloud.google.com/run?project=uga-devel-7447350c) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=uga-devel-7447350c) | [GCP Cloud Scheduler](https://console.cloud.google.com/cloudscheduler?project=uga-devel-7447350c) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=uga-meta-ca2e420e).

## Source code

The source code for the SMI is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Main Application](https://gitlab.developers.cam.ac.uk/uis/devops/uga/smi) | The source code for the main application Docker image |
| [Synchronisation Job Application](https://gitlab.developers.cam.ac.uk/uis/devops/uga/pool-applicant-document-management) | The source code for the synchronisation job application Docker image |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/uga/deploy) | The Terraform infrastructure code for deploying the applications to GCP |

!!! info
    The synchronisation job application repository is named the "Pool Applicant Document Management"
    repository but the name is misleading as the application manages synchronisation with CamSIS,
    Google Drive and Google Sheets, and not just for applicant pooling purposes.

## Technologies used

The following gives an overview of the technologies the SMI is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Web Application Backend | Python 3.7 | Django 2.2 |
| Web Application Frontend | JavaScript | React 16.13.1 |
| Synchronisation Job Application | Python 3.8 | Flask |
| Database | PostgreSQL 11 | n/a |

## Operational documentation

The following gives an overview of how the SMI is deployed and maintained.

### How and where the SMI is deployed

Database for undergraduate applicant data is a PostgreSQL database hosted by GCP Cloud SQL.
The main web application is a Django backend with React frontend, hosted by GCP Cloud Run.
The synchronisation job application (which provides an API with end-points for synchronising
the SMI database with other services) uses the Flask library, is hosted by GCP Cloud Run and
invoked by GCP Cloud Scheduler.

The SMI infrastucture is deployed using Terraform, with releases of the main application and
synchronisation job application deployed by the GitLab CD pipelines associated with the
[infrastructure deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/uga/deploy).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to deploy the SMI.

### Monitoring

- [GCP Cloud Monitoring](https://console.cloud.google.com/monitoring?project=uga-meta-ca2e420e)
    - For tracking the health of applications in the environments and sending alert emails when
      problems are detected.
- Cloud Logs ([production](https://console.cloud.google.com/logs?project=uga-prod-4ef210b2), [test (staging)](https://console.cloud.google.com/logs?project=uga-test-1f58c76d), [development](https://console.cloud.google.com/logs?project=uga-devel-7447350c))
    - For tracking individual requests/responses to/from the web application and the
      synchronisation job application.

### Debugging

The `README.md` files in each of the source code repositories provide information about debugging
both local and deployed instances of the applications.

### Operation

Applicant data is initially retrieved from CamSIS via a synchronisation job (managed by GCP
Cloud Scheduler which periodically calls the synchronisation job API). Additional annotations
are later added by manually importing the subject master spreadsheet (SMS) and subject-specific
variants (using the
[Django admin application page (staging)](https://staging.subjectmoderationinterface.apps.cam.ac.uk/admin/smi/application/)
for the appropriate environment).

Periodic synchronisation jobs ensure that each applicant has an associated folder on Google
Drive (for storing additional documents). They also ensure that applicant data is consistent
between the SMI and several linked Google Sheets spreadsheets: the expression of interest master
list (EoIML) and poolside meeting outcome spreadsheets (PMOSs). A manually invoked process on
CamSIS uses the SMI web application API to retrieve pooling decisions about applicants, and
update the CamSIS database as necessary.

The flow of applicant data to/from the SMI and other services is summarised by the diagram below.

![Flow of applicant data to/from the SMI](subject-moderation-interface-data-flow.jpg)

Further information is available in the [Operational Documentation for the Undergraduate Admissions process](https://gitlab.developers.cam.ac.uk/uis/devops/uga/operational-documentation).

## Service Management and tech lead

The **service owner** for the SMI is TBD.

The **service manager** for the SMI is TBD.

The **tech lead** for the SMI is [Dave Hart](https://www.lookup.cam.ac.uk/person/crsid/dkh21).

The following engineers have operational experience with the SMI and are able to
respond to support requests or incidents:

* [Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174)
* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
