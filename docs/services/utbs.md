title: UTBS

# University Training Booking System

This page gives an overview of the University Training Booking System (UTBS), describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description
The UTBS provides an admin and booking system for training events to various University institutions.

## Service Status

The UTBS is currently live.

## Contact

Technical queries and support should be directed to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always direct requests to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs/-/issues).

## Environments

The UTBS is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://training.cam.ac.uk](https://training.cam.ac.uk)           | utbs-node1.csi.private.cam.ac.uk |
|             |                                      | utbs-node2.csi.private.cam.ac.uk |
|             |                                      | utbs-nodeq.csi.private.cam.ac.uk |
|             |                                      | utbs-live-db1.srv.uis.private.cam.ac.uk |
|             |                                      | utbs-live-db2.srv.uis.private.cam.ac.uk |
|             |                                      | utbs-live-db3.srv.uis.private.cam.ac.uk |
| Staging     | [https://utbs-test.csx.cam.ac.uk](https://utbs-test.csx.cam.ac.uk)      | utbs-test-node1.csi.private.cam.ac.uk |
|             |                                      | utbs-test-node2.csi.private.cam.ac.uk |
|             |                                      | utbs-test-nodeq.csi.private.cam.ac.uk |
| Training    | [https://utbs-training.csx.cam.ac.uk](https://utbs-training.csx.cam.ac.uk)  | utbs-train-node1.csi.private.cam.ac.uk |
|             |                                      | utbs-train-node2.csi.private.cam.ac.uk |
|             |                                      | utbs-train-nodeq.csi.private.cam.ac.uk |

The training environment is used to train users of the system.

## Source code

The source code for the UTBS is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs) | The source code for the main application server. |
| [UTBS API client](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs-client)|utbs-client provides Java, Python and PHP client code for accessing the UTBS web service API.|

## Technologies used

The following gives an overview of the technologies the UTBS is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web Application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 10.5 |

## Operational documentation

The following gives an overview of how the UTBS is deployed and maintained.

### How and where the UTBS is deployed

### Deploying a new release

Deployed by installing RPMs from the [Bes repo](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#.28S.29RPMS_Available_on_Bes)

### Monitoring

The UTBS is monitored by the UIS infra-sas [nagios service](https://nagios.uis.cam.ac.uk/nagios/).

### Debugging

A docker development environment is available.

## Service Management and tech lead

The **service owner** for the UTBS is [Monica Gonzalez](https://www.lookup.cam.ac.uk/person/crsid/mg356).

The **service manager** for the UTBS is [Monica Gonzalez](https://www.lookup.cam.ac.uk/person/crsid/mg356).

The **tech lead** for the UTBS is  [Paul Badcock](https://www.lookup.cam.ac.uk/person/crsid/prb34).

The following engineers have operational experience with the UTBS and are able to respond to support requests or incidents:

* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
* [Monty Dawson](https://www.lookup.cam.ac.uk/person/crsid/wgd23)


System Dependencies
-------------------
- [Lookup](lookup.md)
- [Raven](raven-ucamwebauth.md)

Contact Addresses
-----------------
- trainingservices@uis.cam.ac.uk