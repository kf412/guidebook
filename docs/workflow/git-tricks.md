# Git tips and tricks

This page collects various `git` tips and tricks which may be useful.

## Merging history from two repos

### Scenario

A new version of a service or a new deployment of an existing service has been
developed in a new repository. We have moved over to using that repository as
the source of truth but want the history of the old respository present in the
new one.

### Example

The [IdP frontend
container](https://gitlab.developers.cam.ac.uk/uis/devops/raven/shib-cloud/idp-frontend-container)
for Shibboleth is now the canonical source of configuration. We want to merge
configuration from the [previous Ansible
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/raven/ansible-shibboleth).

### Recipe

Move to your local clone of
https://gitlab.developers.cam.ac.uk/uis/devops/raven/shib-cloud/idp-frontend-container
and add a new remote:

```console
$ cd path/to/idp-frontend-container
$ git checkout master  # checkout the master branch
$ git pull             # make sure we're up to date
$ git remote add other git@gitlab.developers.cam.ac.uk:uis/devops/raven/ansible-shibboleth.git
$ git fetch other
```

The other repository's master branch is now available at `other/master`. Create
a new branch which will be used to open a MR with the new history:

```console
$ git checkout -b branch-for-mr
```

We want to _merge_ the histories of the old repo into the new but not change any
of the existing files. This is an example of a merge _strategy_. Git calls this
strategy "ours". Perform the merge:

```console
$ git merge --strategy ours other/master
```

Check that no changes have been made by computing a diff against master:

```console
$ git diff master  # should produce no output
```

Check that the other repo's history is present:

```console
$ git log  # should see some comits from the other repo
```

If all is good, push to upstream and open a MR:

```console
$ git push -u origin branch-for-mr
```
