# A human-friendly name for the site as a whole.
site_name: 'DevOps Division Guidebook'

# The canonical URL of the site.
site_url: 'https://guidebook.devops.uis.cam.ac.uk/'

# Site layout
nav:
  - 'Overview': 'index.md'
  - 'Other Documentation': 'external-docs.md'
  - 'Contact':
    - contact/index.md
    - contact/public-keys.md
  - 'Notes':
    - notes/index.md
    - notes/webapp-dev-environment.md
    - notes/automating-google-drive.md
    - notes/gcp-deployments.md
    - notes/google-domain-verification.md
    - notes/gunicorn-tuning.md
    - notes/ucs-dev-group-services.md
    - notes/internal-ca.md
  - 'Workflow':
    - workflow/index.md
    - workflow/gitlab.md
    - workflow/flows.md
    - workflow/merge-requests.md
    - workflow/pipelines.md
    - workflow/onboarding.md
    - workflow/pypi.md
    - workflow/credentials-and-secrets.md
    - workflow/git-tricks.md
  - 'Best Practice':
    - best-practice/index.md
    - best-practice/git.md
    - best-practice/api.md
    - best-practice/javascript.md
    - best-practice/python.md
    - best-practice/webapps.md
    - best-practice/ourtools.md
  - 'Deployment':
    - deployment/index.md
    - deployment/gcp-folders.md
    - deployment/terraform.md
    - deployment/permissions-and-roles.md
    - deployment/secrets.md
    - deployment/dns.md
    - deployment/sql-instances.md
    - deployment/web-applications.md
    - deployment/k8s-clusters.md
    - deployment/monitoring-and-alerting.md
    - deployment/continuous-deployment.md
    - deployment/templates.md
  - 'Our Services':
    - services/index.md
    - 'Education':
      - 'Digital Admissions':
        - services/admissions-portal.md
        - services/subject-moderation-interface.md
      - services/essm-sync.md
      - 'Lecture Capture':
        - services/panopto-deployment.md
        - services/lecture-capture-preferences.md
      - services/software-sales.md
      - services/streaming-media.md
      - services/utbs.md
    - 'Infrastructure and Generic Services':
      - services/api-gateway.md
      - services/covid-pooled-testing.md
      - services/gitlab.md
      - 'Identity and Access Management':
        - services/eduroam-tokens.md
        - 'Card Management':
          - services/legacy-card-db.md
          - services/card-management-system.md
        - services/gsuite.md
        - services/jackdaw.md
        - services/lookup.md
        - services/photo-api.md
        - 'Raven':
          - services/raven-metadata.md
          - services/raven-oauth.md
          - services/passwords.md
          - services/raven-shibboleth.md
          - services/raven-ucamwebauth.md
          - services/raven-stats.md
      - services/information-asset-register.md
      - services/self-service-gateway.md
      - services/tls-certificates.md
      - services/design-system.md
    - 'Research':
      - services/research-dashboard.md
      - services/hta.md
      - services/cloud-research-platform.md
  - 'Working Environment':
    - environment/culture.md
    - environment/meeting-rooms.md
  - 'Glossary': 'glossary.md'

# Links for sufficiently with it people to submit changes.
repo_url: 'https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/'
edit_uri: 'blob/master/docs'
repo_name: 'Developer Hub Project'

# Make us look pretty.
theme:
  name: 'material'
  palette:
    primary: 'teal'
    secondary: 'teal'

markdown_extensions:
  # Support notes, warnings, etc.
  - admonition

  # Allow the use of Pygments to do code highlighting. Do not attempt to guess
  # the language if we don't specify.
  - codehilite:
      guess_lang: false

  # Provide permalinks to help with linking to sections.
  - toc:
      permalink: true

  # Support GitLab/GitHub-style task lists.
  - pymdownx.tasklist:
      custom_checkbox: true

  # Allow code blocks to be nested inside other elements
  - pymdownx.superfences
